#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;

private slots:
    void on_ayudaQGIS_pressed();
    void on_zonasPesqueras_pressed();
    void on_territorioInsular_pressed();
    void on_generacionElectrica_2_pressed();
    void on_generacionElectrica_1_pressed();
    void on_residuosDesechos_pressed();
    void on_mineralesVenezuela_pressed();
    void on_pushButton_2_pressed();
    void on_recursosMinerales_pressed();
    void on_paisesUnasur_pressed();
    void on_paisesAlba_pressed();
    void on_mercosur_pressed();
    void on_limitesVenezuela_pressed();
    void on_dependenciasFederales_pressed();
    void on_pescaFluvial_pressed();
    void on_ejesTuristicos_pressed();
    void on_distribucionProductosPetroleo_pressed();
    void on_cuencasPetroliferas_pressed();
    void on_concentracionPoblacion_2_pressed();
    void on_concentracionPoblacion_pressed();
    void on_abrae_pressed();
    void on_agriculturaVenezuela_pressed();
    void on_territoriosIndigenas_pressed();
    void on_rutasMiranda_pressed();
    void on_emigracionOriente_pressed();
    void on_campanaOriente_pressed();
    void on_liberacionDelSur_pressed();
    void on_granColombia_pressed();
    void on_campanaAdmirable_pressed();
    void on_actionSalir_triggered();
    void on_dominiosVegetacion_pressed();
    void on_cuencasHidrograficas_pressed();
    void on_longitudCosta_pressed();
    void on_tiposClimaticos_pressed();
    void on_mapaRelieve_pressed();
    void on_limitesInternacionales_pressed();
    void on_mapaPolitico_pressed();
    void on_mapaMeridianosParalelos_pressed();
    void on_mapaFisico_pressed();
    void on_viaje3Colon_pressed();
    void on_mapaDeVenezuela_pressed();
    void on_viajesDeColon_pressed();
    void on_mapaVegetacion_pressed();
    void on_mapaSismico_pressed();
    void on_abrirQGIS_pressed();

};

#endif // MAINWINDOW_H
