#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <cstdlib>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_abrirQGIS_pressed()
{
    std::system("qgis /usr/share/contenido-educativo/primero/data/mapa.qgs &");
}

void MainWindow::on_mapaSismico_pressed()
{
    std::system("evince /usr/share/contenido-educativo/primero/data/pdf/funvisis/Macrozonas_de_Venezuela_Carta.pdf &");
}

void MainWindow::on_mapaVegetacion_pressed()
{
    std::system("evince /usr/share/contenido-educativo/primero/data/pdf/igvsb/Vegetacion.pdf &");
}

void MainWindow::on_viajesDeColon_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_1ero/Cuatro_viajes_Cristobal_Colon.png &");
}

void MainWindow::on_mapaDeVenezuela_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_1ero/Mapa_de_Venezuela.png &");
}

void MainWindow::on_viaje3Colon_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_1ero/Tercer_viaje_Cristobal_Colon_Venezuela.png &");
}

void MainWindow::on_mapaFisico_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_1ero/Mapa_fisico_de_la_Republica_Bolivariana_de_Venezuela.png &");
}

void MainWindow::on_mapaMeridianosParalelos_pressed()
{
    std::system("evince /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_1ero/Mapa_Meridianos_y_Paralelos.pdf &");
}

void MainWindow::on_mapaPolitico_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Mapa_politico__de_Venezuela.png &");
}

void MainWindow::on_limitesInternacionales_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Mapa_imites_Internacionales_de_Venezuela.png &");
}

void MainWindow::on_mapaRelieve_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Relieve_Venezuela.png &");
}

void MainWindow::on_tiposClimaticos_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Principales_tipos_climaticos_de_Venezuela.png &");
}

void MainWindow::on_longitudCosta_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Longitud_de_Venezuela_en_la_costa_caribeña.png &");
}

void MainWindow::on_cuencasHidrograficas_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Cuencas_Hidrograficas_de_Venezuela.png &");
}

void MainWindow::on_dominiosVegetacion_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_3er/Dominios_de_vegetacion.png &");
}

void MainWindow::on_actionSalir_triggered()
{
    qApp->quit();
}

void MainWindow::on_campanaAdmirable_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Campaña_Admirable.png &");
}

void MainWindow::on_granColombia_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Gran_Colombia.png &");
}

void MainWindow::on_liberacionDelSur_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Liberacion_del_Sur.png &");
}

void MainWindow::on_campanaOriente_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Ruta_de_la_Campaña_de_Oriente.png &");
}

void MainWindow::on_emigracionOriente_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Ruta_de_la_emigracion_a_Oriente.png  &");
}

void MainWindow::on_rutasMiranda_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Rutas_Miranda.png &");
}

void MainWindow::on_territoriosIndigenas_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Historia_de_Venezuela_3er/Territorios__indigenas.png &");
}

void MainWindow::on_agriculturaVenezuela_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Agricultura_en_Venezuela.png &");
}

void MainWindow::on_abrae_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Areas_bajo_regimen_de_administracion_especial.png &");
}

void MainWindow::on_concentracionPoblacion_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Concentración_de_la_poblacion_en_los_espacios_Costa_Montaña,_Los_Llanos.png &");
}

void MainWindow::on_concentracionPoblacion_2_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Concentracion_poblacional.png &");
}

void MainWindow::on_cuencasPetroliferas_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Cuencas_petroliferas.png &");
}

void MainWindow::on_distribucionProductosPetroleo_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Distribucion_de_productos_derivados_del_petroleo.png &");
}

void MainWindow::on_ejesTuristicos_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Ejes_turisticos.png &");
}

void MainWindow::on_pescaFluvial_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Especies_obtenidas_en_pesca_fluvial.png &");
}

void MainWindow::on_dependenciasFederales_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/INFOGRAFIA_dependencias_federales_de_Venezuela.png &");
}

void MainWindow::on_limitesVenezuela_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Limites_Venezuela__Mar_territorial_y_dependencias.png &");
}

void MainWindow::on_mercosur_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Mercosur.png &");
}

void MainWindow::on_paisesAlba_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Paises_integrantes_del_Alba_2012.png &");
}

void MainWindow::on_paisesUnasur_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Paises_integrantes_de_la_Unasur.png &");
}

void MainWindow::on_recursosMinerales_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Recursos_minerales_y_su_distribucion_en_el_espacio_venezolano.png &");
}

void MainWindow::on_pushButton_2_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Refinerias_venezolanas.png &");
}

void MainWindow::on_mineralesVenezuela_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Representacion_cartografica_de_los_minerales_en_Venezuela.png &");
}

void MainWindow::on_residuosDesechos_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Residuos_y_Desechos.png &");
}

void MainWindow::on_generacionElectrica_1_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Sistema_de_generacion_electrica_Subestaciones.png &");
}

void MainWindow::on_generacionElectrica_2_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Sistemas_de_generacion_electrica.png &");
}

void MainWindow::on_territorioInsular_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Territorio_insular_Francisco_de_Miranda.png &");
}

void MainWindow::on_zonasPesqueras_pressed()
{
    std::system("eog /usr/share/contenido-educativo/primero/data/pdf/me/Mapas_Geografia_de_Venezuela/Zonas_pesqueras.png &");
}

void MainWindow::on_ayudaQGIS_pressed()
{
    std::system("evince /usr/share/contenido-educativo/primero/data/ayuda.pdf &");
}
